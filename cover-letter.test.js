function randomInt(max) {
  return Math.floor(Math.random() * max);
}

const introOptions = ['Hi', 'Hello', 'Hey'];

const freelancers = [
  {
    fullName: 'Lucas Lima',
    examples: [
      {
        areas: ['scrapping'],
        text: `Example 1`,
      },
    ],
    uniqueness: [`Computer Engineer`],
  },
  {
    fullName: 'Marcio Lotufo',
    examples: [
      {
        areas: ['woocommerce', 'frontend', 'wordpress', 'gatsby'],
        text: `Example 2`,
      },
    ],
    uniqueness: [`Engineer`],
  },
];

const callsToAction = [
  `Let's have a quick call (free of charge), so that we can discuss your project in more details!`,
  `Let's have a call (no charges), so you can know more about my services!`,
  `If you have any questions, feel free to ask! (free of charges)`
];

const signOffs = [
  `Best regards,`,
  `Hope to hear from you soon!`,
  `Looking forward to work with you,`,
];

function generateCoverLetter(freelancer, client, exampleId) {
  let intro = introOptions[randomInt(introOptions.length)];
  let cta = callsToAction[randomInt(callsToAction.length)];
  let signoff = signOffs[randomInt(signOffs.length)];

  let example = exampleId
    ? freelancer.examples[exampleId]
    : freelancer.examples[randomInt(freelancer.examples.length)];
  let uniqueness =
    freelancer.uniqueness[randomInt(freelancer.uniqueness.length)];

  return `
    ${intro}${client ? ` ${client}` : ''}! 

    ${example.text}

    ${uniqueness}

    ${cta}

    ${signoff}

    ${freelancer.fullName}
    Lotuz Team
  `;
}

const clients = ['Mark', 'John', 'Samantha', ''];

setInterval(() => {
  console.log(
    generateCoverLetter(
      freelancers[randomInt(freelancers.length)],
      clients[randomInt(clients.length)],
    ),
  );
}, 1000);
