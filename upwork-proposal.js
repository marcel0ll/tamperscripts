// ==UserScript==
// @name         Proposal Sampler
// @namespace    http://tampermonkey.net/
// @version      1.0.5
// @description  try to take over the world!
// @author       marceloll
// @match        https://www.upwork.com/ab/proposals/job/*/apply/
// @grant        none
// ==/UserScript==
//

function randomInt(max) {
  return Math.floor(Math.random() * max);
}

const introOptions = ['Hi', 'Hello', 'Hey'];

const freelancers = [
  {
    fullName: 'Lucas Lima',
    examples: [
      {
        areas: ['scrapping'],
        text: `Example 1`,
      },
    ],
    uniqueness: [`Computer Engineer`],
  },
  {
    fullName: 'Marcio Lotufo',
    examples: [
      {
        areas: ['woocommerce', 'frontend', 'wordpress', 'gatsby'],
        text: `Example 2`,
      },
    ],
    uniqueness: [`Engineer`],
  },
];

const callsToAction = [
  `Let's have a quick call (free of charge), so that we can discuss your project in more details!`,
  `Let's have a call (no charges), so you can know more about my services!`,
  `If you have any questions, feel free to ask! (free of charges)`,
];

const signOffs = [
  `Best regards,`,
  `Hope to hear from you soon!`,
  `Looking forward to work with you,`,
];

function generateCoverLetter(freelancer, client, exampleId) {
  let intro = introOptions[randomInt(introOptions.length)];
  let cta = callsToAction[randomInt(callsToAction.length)];
  let signoff = signOffs[randomInt(signOffs.length)];

  let example = exampleId
    ? freelancer.examples[exampleId]
    : freelancer.examples[randomInt(freelancer.examples.length)];
  let uniqueness =
    freelancer.uniqueness[randomInt(freelancer.uniqueness.length)];

  return `${intro}${client ? ` ${client}` : ''}! 

${example.text}

${uniqueness}

${cta}

${signoff}

${freelancer.fullName}
Lotuz Team`;
}

(function () {
  'use strict';

  // Your code here...
  window.onload = () => {
    setTimeout(() => {
      let $lotuzCover = document.createElement('div');
      let $coverLetter = document.getElementById('coverLetter');

      $lotuzCover.innerHTML = `
        <label> Which developer?
          <select id="lotuz_freelancer">
            <option value="">Select one</option>
            ${freelancers.map((freelancer, i) => {
              return `
                <option value=${i}>${freelancer.fullName}</option>
              `;
            })}
          </select>
        </label>
        <label> Client name?
          <input id="lotuz_client_name" type="text" />
        </label>
        <button id="lotuz_generate">Generate new! </button>
      `;

      $coverLetter.parentElement.prepend($lotuzCover);
      $coverLetter.style.height = '500px';

      document.getElementById('lotuz_generate').onclick = event => {
        event.preventDefault();

        let clientName = document.getElementById('lotuz_client_name').value;
        let freelancer =
          freelancers[document.getElementById('lotuz_freelancer').value];

        document.getElementById('coverLetter').value = generateCoverLetter(
          freelancer,
          clientName,
        );
      };
    }, 1500);
  };
})();
