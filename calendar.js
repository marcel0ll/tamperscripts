// ==UserScript==
// @name         Calendar hours counter
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://calendar.google.com/calendar/r*
// @grant        none
// ==/UserScript==

(function() {
  "use strict";

  function calculate() {
    let els = document.querySelectorAll("[data-eventchip]");

    let results = Array.prototype.slice
      .apply(els)
      .map(el => {
        let jobEl = el.querySelector(".FAxxKc");
        if (!jobEl) return;
        let job = jobEl.innerText
          .split("-")[0]
          .trim()
          .toLowerCase();

        let time = el.textContent.match(/\d?\d(:\d\d)?[ap]m/g);
        time = time.map(t => {
          t = t.trim();
          let ampm = t.substr(-2);
          let value = t.substr(0, t.length - 2).split(":");
          let int = parseInt(value[0]);
          let dot = value.length > 1 ? (parseInt(value[1]) / 15) * 0.25 : 0;
          if (int != 12 && ampm === "pm") int += 12;
          if (int === 12 && ampm === "am") int += 12;

          return int + dot;
        });

        let color = el.style.backgroundColor;
        let paid = color !== "rgb(244, 81, 30)" && color !== "rgb(97, 97, 97)";
        let my = color === "rgb(244, 81, 30)";

        return {
          job,
          time: time[1] - time[0],
          paid,
          my
        };
      })
      .filter(t => t)
      .sort(t => (t.paid ? -1 : 1))
      .reduce(
        (acc, t) => {
          acc[t.job] = acc[t.job] ? acc[t.job] + t.time : t.time;
          if (t.paid && !t.my) acc.paidTime += t.time;
          else if (t.my) acc.myTime += t.time;
          else acc.unpaidTime += t.time;
          return acc;
        },
        { paidTime: 0, unpaidTime: 0, myTime: 0 }
      );

    let html = `<table> ${Object.entries(results)
      .map(t => {
        return `
<tr>
<td style="width: 50%; text-align: left;">${t[0].substring(0, 10)}</td>
<td style="width: 50%; text-align: right;">${t[1]}</td>
</tr>
`;
      })
      .join("")}</table>`;

    return html;
  }

  window.onload = () => {
    let note = document.createElement("div");
    note.style = `
width: 100%;
height: auto;
padding: 0 24px;
`;
    let update = document.createElement("button");
    update.innerText = "update";
    update.onclick = () => {
      note.innerHTML = calculate();
      note.appendChild(update);
    };

    note.innerHTML = calculate();
    note.appendChild(update);

    document.querySelector(".hEtGGf.HDIIVe.sBn5T").appendChild(note);
  };
})();
